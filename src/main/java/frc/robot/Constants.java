/*----------------------------------------------------------------------------*/
/* Copyright (c) 2218 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;
/**
 * Add your docs here.
 */
public class Constants {

    // Motor Ids
        //Drivetrain Falcons
        public final static int rightMasterID = 0;
        public final static int rightSlaveID = 1;

        public final static int leftMasterID = 2;
        public final static int leftSlaveID = 3;
        
        //Shooter
        //Spark Ids
        public final static int rightShooterID = 1;
        public final static int leftShooterID = 2;
        //TalonSRX
        public final static int shooterTiltID = 11;
        public final static int shooterPanID = 10;

        //ColorWheel
        public final static int colorWheelID = 19;
        //Intake
        //VictorSPX
        public final static int intakeID = 2;
        //Stager
        //VictorSPX
        public final static int preStagerID = 0;
        //TalonSRX
        public final static int stagerID = 7;
        //Feeder
        //VictorSPX
        public final static int feederID = 1;

    // Solenoids
        //Drivetrain
            public final static int shiftOut = 4;
            public final static int shiftIn = 3;
        //Prestager 
            public final static int preStagerSolenoid = 0;
        //Intake Arms
            public final static int intakeArmsUp = 2;
            public final static int intakeArmsDown = 1;

    // Controllers
        // Joystick IDs
            public final static int leftStickID = 0;
            public final static int rightStickID = 1;
            // Button IDs
        // Gamepad IDs
            public final static int gamePadID = 2;
            //Joystick IDs
                public final static int gpRightStickY = 5;
                public final static int gpRightStickX = 4;

                public final static int gpLeftStickY = 1;
                public final static int gpLeftStickX = 0;
            // Button IDs
                public final static int buttonA = 1;
                public final static int buttonB = 2;
                public final static int buttonX = 3;
                public final static int buttonY = 4;
                public static final int leftBumper = 5;
                public static final int rightBumper = 6;
                public static final int backButton = 7;
                public static final int startButton = 8;
                public static final int leftStickButton = 9;
                public static final int rightStickButton = 12;

    //Pid Loops
        public final static int timeoutMS = 10;
            //3000RPM Values
            public static final double shooterRkF = 0.000195;//0.000195;
            public static final double shooterRkP = 0.001;//0.001000;//0.000252;//8e-3,9.350e-5;
            public static final double shooterRkI = 0.000001;//0.000001;//0.000001;//0.000002,1.61e-6;
            public static final double shooterRkD = 0.001;//1.500000;//0.002500;//0.009,9.650e-5;
            public static final double shooterRkIZone = 10;
        //3000 RPM Shooter Left Side PID
            public static final double shooterLkF = 0.000195;//0.000195;
            public static final double shooterLkP = 0.001;//0.001000;//0.000252;//8e-3,9.350e-5;
            public static final double shooterLkI = 0.000001;//0.000001;//0.000001;//0.000002,1.61e-6;
            public static final double shooterLkD = 0.001;//1.500000;//0.002500;//0.009,9.650e-5;
            public static final double shooterLkIZone = 10;
        //Stager PID
            public static final double stagerkF = 0.0;
            public static final double stagerkP = 0.50;
            public static final double stagerkI = 0.0;
            public static final double stagerkD = 0.0;
        //Pan PID
            //Slot 0
            public static final double pankF = 0.0;
            public static final double pankP = 0.13;
            public static final double pankI = 0.0;
            public static final double pankD = 1.55;
            //Slot 1
            public static final double pankF1 = 0.0;
            public static final double pankP1 = .22;
            public static final double pankI1 = 0.000001;
            public static final double pankD1 = 1.55;
        //Hood Angle
            public static final double tiltkF = 0.0;
            public static final double tiltkP = .22;
            public static final double tiltkI = 0.000001;
            public static final double tiltkD = 1.55;

    //Sensor IDs
        public final static int sensorOneID = 1;
        public final static int sensorFourID = 4;
        public final static int sensorFiveID = 5;
        public final static int sensorFourPointFiveID = 0;
}

