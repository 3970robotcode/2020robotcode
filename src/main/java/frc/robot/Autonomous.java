/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import com.ctre.phoenix.motorcontrol.ControlMode;

//package org.usfirst.frc.team3970.robot;
import frc.robot.subsystems.*;
//import com.ctre.phoenix.motorcontrol.ControlMode;

//import edu.wpi.first.wpilibj.Timer;

public class Autonomous {
	Drivetrain robotDrive;
	Intake robotIntake;
	Shooter robotShooter;
	Stager robotStager;

	int stateDelay;
	public int currentState;
	
	//int state;
	Autonomous(Drivetrain drivetrain, Intake intake, Shooter shooter, Stager stager){
		robotDrive = drivetrain;
		robotIntake = intake;
		robotShooter = shooter;
		robotStager = stager;
		currentState = 0;
	}

	public void setState(int nextState){
		currentState = nextState;
	}

	void basicAuto() {
		if(currentState == 0){
			if(robotDrive.goForwardTimed(robotDrive, 0.5, 0.35)){ //307	
				setState(-1);
            }
        }
	}

	void autoRoutineOne() {
		stateDelay += 20;
		if (currentState == 0) {
			if(robotShooter.setHoodAngle(15) && robotShooter.visionTrackingAuto()) {
				setState(1);
			}
			robotShooter.shooterRPM(3000);
		}
		else if(currentState == 1) {   
			if(robotShooter.shootAuto(3)){
				System.out.println(robotShooter.shootAuto(3));
				robotShooter.rightShooter.set(0);
				robotShooter.leftShooter.set(0);
				robotStager.feederMotor.set(ControlMode.PercentOutput, 0.0);
            	robotStager.stager.set(ControlMode.PercentOutput, 0.0);
            	robotStager.preStager.set(ControlMode.PercentOutput, 0.0);
				setState(2);
			}
		}
		else if(currentState == 2) {
			if(robotDrive.driveToPosition(50)){
				robotDrive.encoderReset();
				setState(-1);
			}
		}

	}
}


	


