/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import frc.robot.Constants;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Solenoid;
//import edu.wpi.first.wpilibj.Timer;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;


/**
 * Add your docs here.
 */
public class Stager{

    public VictorSPX preStager;
    public TalonSRX stager;
    public VictorSPX feederMotor;

    public Solenoid preStagerSolenoid;

    public DigitalInput position1;
    public DigitalInput position2;
    public DigitalInput position3;
    public DigitalInput position4;
    public DigitalInput position5;

    Joystick gamePad;

    boolean ballPos;

    boolean moveComplete1, moveComplete2, feed, moveComplete3; //Can we get rid of moveComplete 1 and 2 and change moveComplete 3 to moveComplete

    Intake robotIntake;


    public Stager(Intake intake, Joystick gp) {
        preStagerSolenoid = new Solenoid(Constants.preStagerSolenoid);
        robotIntake = intake;
        feederMotor = new VictorSPX(Constants.feederID);
        gamePad = gp;
        // We should go through and set all the negative motors invered so they all read positive
        preStager = new VictorSPX(Constants.preStagerID);
        stager = new TalonSRX(Constants.stagerID);
        moveComplete1 = true;
        moveComplete2 = true;
        moveComplete3 = true;
        feed = false;
        stager.configSelectedFeedbackSensor(FeedbackDevice.QuadEncoder);
        stager.config_kF(0, Constants.stagerkF);
        stager.config_kP(0, Constants.stagerkP);
        stager.config_kI(0, Constants.stagerkI);
        stager.config_kD(0, Constants.stagerkD);
        stager.setSelectedSensorPosition(0);
        stager.setSensorPhase(true);

        position1 = new DigitalInput(Constants.sensorOneID);
        //position2 = new DigitalInput(5);
        //position3 = new DigitalInput(2);
        position4 = new DigitalInput(Constants.sensorFourID);
        position5 = new DigitalInput(Constants.sensorFiveID);

    }

    public void compBot(boolean isCompBot){ 
        if(isCompBot){
            robotIntake.intakeMotor.setInverted(true);
            feederMotor.setInverted(true);
        }
        else {
            robotIntake.intakeMotor.setInverted(false);
            feederMotor.setInverted(false);
        }
    }

    public void ballInput(){ //We should revamp this so we can use all 5 variables without mounting the other sensors
        SmartDashboard.putBoolean("Position1", position1.get());
        SmartDashboard.putBoolean("Position2", position2.get());
        SmartDashboard.putBoolean("Position3", position3.get());
        SmartDashboard.putBoolean("Position4", position4.get());
        SmartDashboard.putBoolean("Position5", position5.get());
    }

    public void stagerTeleop(){
        SmartDashboard.putNumber("Stager Encoder", stager.getSelectedSensorPosition());

        if(gamePad.getRawButton(Constants.backButton)){
            preStager.set(ControlMode.PercentOutput, 0.75);
            robotIntake.intakeMotor.set(ControlMode.PercentOutput,-0.75);
            stager.set(ControlMode.PercentOutput, -0.5);
            feederMotor.set(ControlMode.PercentOutput, 0.75);
            stager.setSelectedSensorPosition(0);
        } 
        
        else if(gamePad.getRawButton(Constants.startButton)){
            robotIntake.intakeMotor.set(ControlMode.PercentOutput, -0.75);
        }

        else if(gamePad.getRawButton(Constants.buttonB)) {
            SmartDashboard.putNumber("Stager Encoder", stager.getSelectedSensorPosition()); //Get rid of this line, it's a duplicate from two if statements up

            if(!position5.get() && !position1.get() && !position4.get()) {
                preStager.set(ControlMode.PercentOutput, 0);
                robotIntake.intakeMotor.set(ControlMode.PercentOutput, -.75);
                preStager.set(ControlMode.PercentOutput, 0);
                //robotIntake.intakeMotor.set(ControlMode.PercentOutput,0);
                feederMotor.set(ControlMode.PercentOutput, 0); //change this to 0.3, which stops the next ball from pushing it up
                stager.set(ControlMode.PercentOutput, 0);
            }
            else {
                preStager.set(ControlMode.PercentOutput, -.5);
                robotIntake.intakeMotor.set(ControlMode.PercentOutput, 1.00); 
            }
            
            if(!moveComplete3) {
                if(!position4.get() && !position5.get()) {
                    stager.set(ControlMode.PercentOutput, 0);
                }
                else{
                    stager.set(ControlMode.Position, 3000);
                    feederMotor.set(ControlMode.PercentOutput, 0);
                    if(stager.getSelectedSensorPosition() > 2900){
                        moveComplete3 = true;
                    }
                    else {
                        moveComplete3 = false;
                    } 
                }
            }

            //Can we rewrite this as (I think this might be why it's trying to push the last ball up too far sometimes): 
            /*
            if(!position4.get() && position5.get()) {
                feederMotor.set(ControlMode.PercentOutput, -0.25);
            }
            else {
                feederMotor.set(ControlMode.PercentOutput, .03);
            }
            */
            if(!position5.get()) {
                feederMotor.set(ControlMode.PercentOutput, .03);
            }
            if(!position4.get()) {
                if (position5.get()) { 
                    feederMotor.set(ControlMode.PercentOutput, -.25);
                }
                else { 
                    feederMotor.set(ControlMode.PercentOutput, .03);
                }
            }

            if(!position1.get()) {
                stager.setSelectedSensorPosition(0);
                moveComplete3 = false;
            }
        }
        else if (!gamePad.getRawButton(Constants.buttonA)){
            robotIntake.intakeMotor.set(ControlMode.PercentOutput, 0);
            preStager.set(ControlMode.PercentOutput, 0);
            stager.set(ControlMode.PercentOutput, 0);
            feederMotor.set(ControlMode.PercentOutput, 0);
        }

    }


    public void overrideStagerFunction(){ //This is included in the stager teleop function
        if(gamePad.getRawButton(Constants.backButton)){
            stager.set(ControlMode.PercentOutput, 0.50);
            feederMotor.set(ControlMode.PercentOutput, 0.50);
        }
    }
    public void stagerSimple(){

        if(gamePad.getRawButton(Constants.startButton)){
            preStager.set(ControlMode.PercentOutput, -0.5);
            robotIntake.intakeMotor.set(ControlMode.PercentOutput,1);
            stager.set(ControlMode.PercentOutput, 0.5);
            feederMotor.set(ControlMode.PercentOutput, -.75);
        }
        else if(gamePad.getRawButton(Constants.backButton)){
            preStager.set(ControlMode.PercentOutput, 0.5);
            robotIntake.intakeMotor.set(ControlMode.PercentOutput,-0.75);
            stager.set(ControlMode.PercentOutput, -0.5);
            feederMotor.set(ControlMode.PercentOutput, .75);
        }
        else {
            preStager.set(ControlMode.PercentOutput, 0.0);
            robotIntake.intakeMotor.set(ControlMode.PercentOutput,0.0);
            stager.set(ControlMode.PercentOutput, 0.0);
            feederMotor.set(ControlMode.PercentOutput, 0.0);
        }   
    }
    public void preStagerUp(){
        preStagerSolenoid.set(true);
    }
    
}