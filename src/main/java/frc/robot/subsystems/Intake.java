package frc.robot.subsystems;

import frc.robot.Constants;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Joystick;

public class Intake {

    public VictorSPX intakeMotor;
    public DoubleSolenoid intakeUpAndDown;
    public Joystick gamePad;
    public boolean isShooterDown = false;

    public Intake(Joystick gp){
        gamePad = gp;
        intakeUpAndDown = new DoubleSolenoid(Constants.intakeArmsDown, Constants.intakeArmsUp);
        intakeMotor = new VictorSPX(Constants.intakeID);
    }

    public void intakeArmsTeleop(){
        if(gamePad.getRawButton(Constants.buttonY)){
            intakeUpAndDown.set(DoubleSolenoid.Value.kReverse);
            isShooterDown = false;
        }
        else if(gamePad.getRawButton(Constants.buttonX)){
            intakeUpAndDown.set(DoubleSolenoid.Value.kForward);
            isShooterDown = true;
        }

    }
}
