/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import frc.robot.Constants;

import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;

import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import com.revrobotics.CANEncoder;
import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import com.revrobotics.ControlType;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.CANSparkMaxLowLevel.PeriodicFrame;
/**
 * Add your docs here.
 */
public class Shooter {

    public CANSparkMax rightShooter, leftShooter;
    
    public TalonSRX shooterPan, hoodAngle;
    
    public CANPIDController leftShooterPID, rightShooterPID;
    
    public CANEncoder leftShooterEncoder, rightShooterEncoder;

    public DigitalInput position4dot5;
    
    Stager robotStager;
    Joystick gamePad;
    
    double visionX;
    double rpmSetpoint;
    
    boolean moveComplete;
    boolean moveComplete1;
    boolean moveComplete2;

    boolean delayToggle;
    boolean velocityToggle;
    boolean isShot;
    
    int maxSetPoint;
    int minSetPoint;
    int shotDelayCount;
    public int autoDelayCount;
    public int shotCount;

    public Shooter(Joystick gp, Stager stager){
        moveComplete2 = true;
        moveComplete1 = true;

        velocityToggle = false;
        delayToggle = false;
        isShot = false;

        shotCount = 0;
        shotDelayCount = 0;
        autoDelayCount = 0;

        maxSetPoint = -27500;
        minSetPoint = 3000;

        gamePad = gp;
        robotStager = stager;

        position4dot5 = new DigitalInput(Constants.sensorFourPointFiveID);

        rightShooter = new CANSparkMax(Constants.rightShooterID, MotorType.kBrushless);
        leftShooter = new CANSparkMax(Constants.leftShooterID, MotorType.kBrushless);

        shooterPan = new TalonSRX(Constants.shooterPanID);
        hoodAngle = new TalonSRX(Constants.shooterTiltID);

        rightShooter.setPeriodicFramePeriod(PeriodicFrame.kStatus1, 20);
        leftShooter.setPeriodicFramePeriod(PeriodicFrame.kStatus1, 20);

        rightShooter.setInverted(false);
        leftShooter.setInverted(true);

        rightShooterPID = rightShooter.getPIDController();
        leftShooterPID = leftShooter.getPIDController();

        leftShooterEncoder = leftShooter.getEncoder();
        rightShooterEncoder = rightShooter.getEncoder();

        rightShooterPID.setP(Constants.shooterRkP);
        leftShooterPID.setP(Constants.shooterLkP);

        rightShooterPID.setI(Constants.shooterRkI);
        leftShooterPID.setI(Constants.shooterLkI);

        rightShooterPID.setD(Constants.shooterRkD);
        leftShooterPID.setD(Constants.shooterLkD);

        rightShooterPID.setFF(Constants.shooterRkF);
        leftShooterPID.setFF(Constants.shooterLkF);

        rightShooterPID.setIZone(Constants.shooterRkIZone);
        leftShooterPID.setIZone(Constants.shooterLkIZone);

        shooterPan.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 20);
        shooterPan.setSensorPhase(true);

        shooterPan.config_kF(0, Constants.pankF);
        shooterPan.config_kP(0, Constants.pankP);
        shooterPan.config_kI(0, Constants.pankI);
        shooterPan.config_kD(0, Constants.pankD);

        shooterPan.config_kF(1, Constants.pankF1);
        shooterPan.config_kP(1, Constants.pankP1);
        shooterPan.config_kI(1, Constants.pankI1);
        shooterPan.config_kD(1, Constants.pankD1);

        shooterPan.configPeakOutputForward(3);
        shooterPan.configPeakOutputReverse(-3);

        hoodAngle.config_kF(0, Constants.tiltkF);
        hoodAngle.config_kP(0, Constants.tiltkP);
        hoodAngle.config_kI(0, Constants.tiltkI);
        hoodAngle.config_kD(0, Constants.tiltkD);
        
        shooterPan.setSelectedSensorPosition(0);
        hoodAngle.setSelectedSensorPosition(0);
    }

    public void shootTeleop() { 
        if(gamePad.getRawButton(Constants.buttonA)){
            shotDelayCount += 20;

            if(NetworkTableInstance.getDefault().getTable("limelight").getEntry("tv").getDouble(0) < 1){
                rpmSetpoint= 3000;
                shooterRPM(rpmSetpoint);
                setHoodAngle(0);
            }
            else {
                rpmSetpoint= 3000;
                shooterRPM(rpmSetpoint);
                setHoodAngle(15);
            }
            
            if((rightShooterEncoder.getVelocity() >= rpmSetpoint-5)){
                velocityToggle = true;
            }

            if(shotDelayCount >= 50 && !delayToggle){
                shotDelayCount = 0;
                delayToggle = true;
            }
            else if(shotDelayCount >= 200 && delayToggle){
                shotDelayCount = 0; 
                delayToggle = false;
            }

            if(position4dot5.get()){
                robotStager.feederMotor.set(ControlMode.PercentOutput, -.75);
                robotStager.stager.set(ControlMode.PercentOutput, .75);
                robotStager.preStager.set(ControlMode.PercentOutput, -.75);
            }
            else if(!gamePad.getRawButton(Constants.rightBumper) && delayToggle && velocityToggle) {
                //shootToggle = true;
                robotStager.feederMotor.set(ControlMode.PercentOutput, -1);
                robotStager.stager.set(ControlMode.PercentOutput, 1);
                robotStager.preStager.set(ControlMode.PercentOutput, 1);
            }
            else if(!position4dot5.get()) {
                robotStager.feederMotor.set(ControlMode.PercentOutput, 0.0);
                robotStager.stager.set(ControlMode.PercentOutput, 0.0);
                robotStager.preStager.set(ControlMode.PercentOutput, 0.0);
            }
            else{
                robotStager.feederMotor.set(ControlMode.PercentOutput, 0.0);
                robotStager.stager.set(ControlMode.PercentOutput, 0.0);
                robotStager.preStager.set(ControlMode.PercentOutput, 0.0);
            }
        }
        else{
            velocityToggle = false;
            shotDelayCount = 0;
            rightShooter.setVoltage(0);
            leftShooter.setVoltage(0);
        }
    }

    public boolean shootAuto(int ballCount){
        shotDelayCount += 20;

        autoDelayCount += 20;
        
        rpmSetpoint= 3000;
        shooterRPM(rpmSetpoint);

        if(autoDelayCount >= 5000 || ballCount+1 <= shotCount){
            return true;
        }

        if(!isShot && robotStager.position5.get()) {
            isShot = true;
            shotCount++;
        }
        else if(!robotStager.position5.get()) {
            isShot = false;
        }

        if(shotDelayCount >= 50 && !delayToggle){
            shotDelayCount = 0;
            delayToggle = true;
        }
        else if(shotDelayCount >= 200 && delayToggle){
            shotDelayCount = 0; 
            delayToggle = false;
        }
        
        if((rightShooterEncoder.getVelocity() >= rpmSetpoint-5)){
            velocityToggle = true;
        }

        if(position4dot5.get()){
            robotStager.feederMotor.set(ControlMode.PercentOutput, -.75);
            robotStager.stager.set(ControlMode.PercentOutput, .75);
            robotStager.preStager.set(ControlMode.PercentOutput, -.75);
        }
        else if(!gamePad.getRawButton(Constants.rightBumper) && delayToggle && velocityToggle) {
            //shootToggle = true;
            robotStager.feederMotor.set(ControlMode.PercentOutput, -1);
            robotStager.stager.set(ControlMode.PercentOutput, 1);
            robotStager.preStager.set(ControlMode.PercentOutput, 1);
        }
        else if(!position4dot5.get()) {
            robotStager.feederMotor.set(ControlMode.PercentOutput, 0.0);
            robotStager.stager.set(ControlMode.PercentOutput, 0.0);
            robotStager.preStager.set(ControlMode.PercentOutput, 0.0);
        }
        else{
            robotStager.feederMotor.set(ControlMode.PercentOutput, 0.0);
            robotStager.stager.set(ControlMode.PercentOutput, 0.0);
            robotStager.preStager.set(ControlMode.PercentOutput, 0.0);
        }
        return false;
    }

    public void visionTrackingTeleop(){
        visionX = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tx").getDouble(0);
        SmartDashboard.putNumber("X Offset", visionX);

        if(gamePad.getRawButton(Constants.leftBumper)){
            if(shooterPan.getSelectedSensorPosition() < maxSetPoint || !moveComplete2){
                shooterPan.selectProfileSlot(0, 0);
                moveComplete2 = false;
                shooterPan.set(ControlMode.Position, 2000);

                if(shooterPan.getSelectedSensorPosition() > 1000 ) {
                    moveComplete2 = true;
                }
                else {
                    moveComplete2 = false;
                }
            }
            else if (shooterPan.getSelectedSensorPosition() > minSetPoint || !moveComplete1) {
                shooterPan.selectProfileSlot(0, 0);
                moveComplete1 = false;
                shooterPan.set(ControlMode.Position, -27000);

                if(shooterPan.getSelectedSensorPosition() < -26000) {
                    moveComplete1 = true;
                }
                else {
                    moveComplete1 = false;
                }
            }
            else{
                shooterPan.selectProfileSlot(1, 0);
                shooterPan.set(ControlMode.Position, shooterPan.getSelectedSensorPosition()-(visionX*(15275/180)));
            }
        }
        else{
            shooterPan.set(ControlMode.Position, 0);
        }
    }

    public boolean visionTrackingAuto(){
        visionX = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tx").getDouble(0);
        shooterPan.selectProfileSlot(1, 0);
        shooterPan.set(ControlMode.Position, shooterPan.getSelectedSensorPosition()-(visionX*(15275/180)));
        if(Math.abs(shooterPan.getClosedLoopError()) < 100){
            return true;
        }
        return false;
    }

    public void turretPIDTest(){
        shooterPan.config_kF(0, SmartDashboard.getNumber("FF", 0.0));
        shooterPan.config_kP(0, SmartDashboard.getNumber("P", 0.0));
        shooterPan.config_kI(0, SmartDashboard.getNumber("I", 0.0));
        shooterPan.config_kD(0, SmartDashboard.getNumber("D", 0.0));
        
        if(gamePad.getRawButton(Constants.buttonA)){
            shooterPan.set(ControlMode.Position, -1000);
            System.out.println(shooterPan.getClosedLoopError());
        }
        else if(gamePad.getRawButton(Constants.rightBumper)){
            shooterPan.setSelectedSensorPosition(0); 
        }
    }

    public void shooterSimple(){
        if(gamePad.getRawButton(Constants.rightBumper)){
            rightShooter.set(-1);
            leftShooter.set(-1);
        }
        else{
            rightShooter.set(0.0);
            leftShooter.set(0.0);
        }
    }

    public void manualPanAndTilt(){
        hoodAngle.set(ControlMode.PercentOutput, gamePad.getRawAxis(Constants.gpRightStickX));
    }

    public boolean setHoodAngle(double angle){
        hoodAngle.set(ControlMode.Position, -angle*(43904/27));
        if(Math.abs(hoodAngle.getClosedLoopError()) < 150){
            return true;
        }
        return false;
    }

    public void shooterRPM(double shooterRPM){
        leftShooterPID.setReference(shooterRPM, ControlType.kVelocity);
        rightShooterPID.setReference(shooterRPM, ControlType.kVelocity);
    }

    public double getDX(){
        return 6.2/Math.tan((35+NetworkTableInstance.getDefault().getTable("limelight").getEntry("ty").getDouble(0))*(Math.PI / 180));
    }
}