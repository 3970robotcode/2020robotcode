/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;
import frc.robot.Constants;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * Add your docs here.
 */
public class Drivetrain {
    public TalonFX driveLeftMaster;
	public TalonFX driveLeftSlave1;

	public TalonFX driveRightMaster;
    public TalonFX driveRightSlave1;

    public ADXRS450_Gyro driveTrainGyro;
    
    public DoubleSolenoid driveShift;

    Joystick rightStick;
    Joystick leftStick;
    Joystick gamePad;

    double angleSetPoint;
    double angle;

    double inches;
    double rotations;
    double targetDistance;

    public Drivetrain(Joystick rs, Joystick ls, Joystick gp){
        rightStick = rs;
        leftStick = ls; 
        gamePad = gp;
        driveLeftMaster = new TalonFX(Constants.leftMasterID);
        driveLeftSlave1 = new TalonFX(Constants.leftSlaveID);

        driveRightMaster = new TalonFX(Constants.rightMasterID);
        driveRightSlave1 = new TalonFX(Constants.rightSlaveID);

        driveTrainGyro = new ADXRS450_Gyro();

        driveLeftMaster.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor, 0, 10);
        driveLeftSlave1.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor, 0, 10);
        driveRightSlave1.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor, 0, 10);
        driveRightSlave1.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor, 0, 10);

        driveLeftMaster.config_kF(0, 0);
        driveLeftMaster.config_kP(0, 0.35);
        driveLeftMaster.config_kI(0, 0.002);
        driveLeftMaster.config_kD(0, 3.5);

        driveRightMaster.config_kF(0, 0);
        driveRightMaster.config_kP(0, 0.35);
        driveRightMaster.config_kI(0, 0.002);
        driveRightMaster.config_kD(0, 3.5);

        driveLeftSlave1.set(ControlMode.Follower, Constants.leftMasterID);
        driveRightSlave1.set(ControlMode.Follower, Constants.rightMasterID);

        driveShift = new DoubleSolenoid(Constants.shiftIn, Constants.shiftOut);
    }

    public void teleop(){
        driveLeftMaster.set(ControlMode.PercentOutput, 1*(leftStick.getY()));
        driveLeftSlave1.set(ControlMode.PercentOutput, 1*(leftStick.getY()));
        driveRightMaster.set(ControlMode.PercentOutput, -1*(rightStick.getY()));
        driveRightSlave1.set(ControlMode.PercentOutput, -1*(rightStick.getY()));
        if(leftStick.getRawButton(6)){
            driveShift.set(DoubleSolenoid.Value.kForward);
        }
        else if(rightStick.getRawButton(11)){
            driveShift.set(DoubleSolenoid.Value.kReverse);
        }
        SmartDashboard.putNumber("Right Master Temp", driveRightMaster.getTemperature());
        SmartDashboard.putNumber("Right Slave Temp", driveRightSlave1.getTemperature());
        SmartDashboard.putNumber("Left Master Temp", driveLeftMaster.getTemperature());
        SmartDashboard.putNumber("Left Slave Temp", driveLeftSlave1.getTemperature());
    }

    public void teleopProgramming(){
        driveLeftMaster.set(ControlMode.PercentOutput, 1*(gamePad.getRawAxis(Constants.gpLeftStickY)));
        driveLeftSlave1.set(ControlMode.PercentOutput, 1*(gamePad.getRawAxis(Constants.gpLeftStickY)));
        driveRightMaster.set(ControlMode.PercentOutput, -1*(gamePad.getRawAxis(Constants.gpRightStickY)));
        driveRightSlave1.set(ControlMode.PercentOutput, -1*(gamePad.getRawAxis(Constants.gpRightStickY)));
        if(leftStick.getRawButton(6)){
            driveShift.set(DoubleSolenoid.Value.kForward);
        }
        else if(rightStick.getRawButton(11)){
            driveShift.set(DoubleSolenoid.Value.kReverse);
        }
        SmartDashboard.putNumber("Right Master Temp", driveRightMaster.getTemperature());
        SmartDashboard.putNumber("Right Slave Temp", driveRightSlave1.getTemperature());
        SmartDashboard.putNumber("Left Master Temp", driveLeftMaster.getTemperature());
        SmartDashboard.putNumber("Left Slave Temp", driveLeftSlave1.getTemperature());
    }

    public void teleopRamped(){
        System.out.println(1*(joystickRamp(leftStick.getY())));
        driveLeftMaster.set(ControlMode.PercentOutput, 1*(joystickRamp(leftStick.getY())));
        driveLeftSlave1.set(ControlMode.PercentOutput, 1*(joystickRamp(leftStick.getY())));
        driveRightMaster.set(ControlMode.PercentOutput, -1*(joystickRamp(rightStick.getY())));
        driveRightSlave1.set(ControlMode.PercentOutput, -1*(joystickRamp(rightStick.getY())));
        if(leftStick.getRawButton(6)){
            driveShift.set(DoubleSolenoid.Value.kForward);
        }
        else if(rightStick.getRawButton(11)){
            driveShift.set(DoubleSolenoid.Value.kReverse);
        } 
    }

    public boolean goForwardTimed(Drivetrain robotDrive, double time, double output){
        robotDrive.driveLeftMaster.set(ControlMode.PercentOutput, -1.0*output);
        robotDrive.driveRightMaster.set(ControlMode.PercentOutput, output);
        Timer.delay(time);
        robotDrive.driveLeftMaster.set(ControlMode.PercentOutput, -0.0);
        robotDrive.driveRightMaster.set(ControlMode.PercentOutput, 0.0);
        return true;
    }

    public boolean turnToDegree(double angleSetPoint){
        driveRightMaster.set(ControlMode.PercentOutput, GyroMotorOutput(angleSetPoint, 0.009, 0.15));
        driveLeftMaster.set(ControlMode.PercentOutput, GyroMotorOutput(angleSetPoint, 0.009, 0.15));
        if(GyroMotorOutput(angleSetPoint, 0.009, 0.15) == 0){
            return true;
        }
        else {
            return false;
        }
    }

    public double GyroMotorOutput(double angleSetPoint, double kP, double tolerance) {
        angle = driveTrainGyro.getAngle();
        final double error = angleSetPoint + angle;
        if(Math.abs(error) > tolerance){
            System.out.println("Error" );
            System.out.println(error);
            System.out.println("Motor Output");
            System.out.println(error*kP + Math.signum(error)*0.10);
            return error*kP + Math.signum(error)*0.10;
            
        }
        else{
            System.out.println("Turn Complete");
            System.out.println("Error" );
            System.out.println(error);
            return 0; 

        }
    }

    public boolean driveToPosition(double inches){
        rotations = inches / (6 * Math.PI);
        targetDistance = rotations * 30720;

        driveLeftMaster.set(ControlMode.Position, targetDistance);
        driveLeftSlave1.set(ControlMode.Position, targetDistance);
        driveRightMaster.set(ControlMode.Position, -targetDistance);
        driveRightSlave1.set(ControlMode.Position, -targetDistance);

        if(driveLeftMaster.getSelectedSensorPosition() <= targetDistance+10 && driveLeftMaster.getSelectedSensorPosition() >= targetDistance-10){
            driveRightMaster.set(ControlMode.PercentOutput, 0);
            driveLeftMaster.set(ControlMode.PercentOutput, 0);
            driveRightSlave1.set(ControlMode.PercentOutput, 0);
            driveLeftSlave1.set(ControlMode.PercentOutput, 0);
            return true;
        }
        else{
            return false;
        }
    }

    public void driveTrainEncoderTest(){
        if(gamePad.getRawButton(Constants.buttonA)){
            driveLeftMaster.set(ControlMode.Position, 30720);
            driveRightMaster.set(ControlMode.Position, 30720);
        }
        else{
            driveLeftMaster.set(ControlMode.PercentOutput, 0);
            driveLeftSlave1.set(ControlMode.PercentOutput, 0);
            driveRightMaster.set(ControlMode.PercentOutput, 0);
            driveRightSlave1.set(ControlMode.PercentOutput, 0);
        }
        if(gamePad.getRawButton(Constants.buttonB)){
            driveLeftMaster.setSelectedSensorPosition(0);
            driveLeftSlave1.setSelectedSensorPosition(0);
            driveRightMaster.setSelectedSensorPosition(0);
            driveRightSlave1.setSelectedSensorPosition(0);
        }
    }
    
    public double joystickRamp(double stickValue){
        return Math.signum(stickValue)*(stickValue*stickValue);
    }

    public void encoderReset(){
        driveLeftMaster.setSelectedSensorPosition(0);
    }
}
