/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import frc.robot.subsystems.*;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

//import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.wpilibj.Compressor;
// com.ctre.phoenix.motorcontrol.ControlMode;
import edu.wpi.first.wpilibj.TimedRobot;
// import edu.wpi.first.wpilibj.Timer;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  private static final String routine1 = "Routine1";
  private static final String  basicAuto= "basicAuto";
  private String selectedAuto;

  private final SendableChooser<String> chooser = new SendableChooser<>();

  Joystick gamePad;
  Joystick rightStick;
  Joystick leftStick;
  Drivetrain robotDrive;
  Autonomous auto;
  Shooter robotShooter;
  Intake robotIntake;
  Stager robotStager;
  Compressor airCompressor;
  boolean isCompbot;

  @Override
  public void robotInit() {
    chooser.setDefaultOption("Routine 1", routine1);
    chooser.addOption("Basic Auto", basicAuto);

    SmartDashboard.putData("Auto choices", chooser);

    gamePad = new Joystick(Constants.gamePadID);
    rightStick = new Joystick(Constants.leftStickID);
    leftStick = new Joystick(Constants.rightStickID);

    robotDrive = new Drivetrain(rightStick, leftStick, gamePad);
    robotIntake = new Intake(gamePad);
    robotStager = new Stager(robotIntake, gamePad);
    robotShooter = new Shooter(gamePad, robotStager);

    auto = new Autonomous(robotDrive, robotIntake, robotShooter, robotStager);

    airCompressor = new Compressor(0);  
    airCompressor.setClosedLoopControl(true);
    airCompressor.start();
    
    robotStager.compBot(true);
  }

  public void robotPeriodic(){   
  }

  @Override
  public void autonomousInit() {
    auto.currentState = 0;
    robotShooter.shotCount = 0;
    robotShooter.autoDelayCount = 0;
    robotDrive.driveLeftMaster.setSelectedSensorPosition(0);
    selectedAuto = chooser.getSelected();
  }

  @Override
  public void autonomousPeriodic() {
    switch (selectedAuto) {
      case routine1:
        auto.autoRoutineOne();
        break;
      case basicAuto:
        auto.basicAuto();
        break;
    }   
    SmartDashboard.putNumber("State", auto.currentState);
  }

  @Override
  public void teleopInit() {
    robotStager.preStagerUp();
    /*
    SmartDashboard.putNumber("FF", 0); //.05
    SmartDashboard.putNumber("P", 0); //1.0e-4)
    SmartDashboard.putNumber("I", 0);
    SmartDashboard.putNumber("D", 0);
    SmartDashboard.putNumber("I Zone", 0);
    SmartDashboard.putNumber("Setpoint", 0);
    */
  }


  @Override
  public void teleopPeriodic() {
    robotDrive.teleop();
    robotStager.stagerTeleop();
    robotIntake.intakeArmsTeleop();
    robotShooter.shootTeleop();

    //robotDrive.driveTrainEncoderTest(); 

    //robotShooter.shooterPan.set(ControlMode.Position, 0);
    //robotShooter.setHoodAngle(15);
    
    /*robotShooter.rightShooterPID.setP(SmartDashboard.getNumber("P", 0));
    robotShooter.leftShooterPID.setP(SmartDashboard.getNumber("P", 0));

    robotShooter.rightShooterPID.setI(SmartDashboard.getNumber("I", 0));
    robotShooter.leftShooterPID.setI(SmartDashboard.getNumber("I", 0));

    robotShooter.rightShooterPID.setD(SmartDashboard.getNumber("D", 0));
    robotShooter.leftShooterPID.setD(SmartDashboard.getNumber("D", 0));

    robotShooter.rightShooterPID.setFF(SmartDashboard.getNumber("FF", 0));
    robotShooter.leftShooterPID.setFF(SmartDashboard.getNumber("FF", 0));

    robotShooter.rightShooterPID.setIZone(SmartDashboard.getNumber("I Zone", 0));
    robotShooter.leftShooterPID.setIZone(SmartDashboard.getNumber("I Zone", 0));*/
    
    /*if(gamePad.getRawButton(Constants.buttonA)){
      robotShooter.motorRPM(1530);
    }
    else{
      robotShooter.rightShooter.set(0);
      robotShooter.leftShooter.set(0);
    }*/

    SmartDashboard.putBoolean("Sensor 1", !robotStager.position1.get());
    SmartDashboard.putBoolean("Sensor 4", !robotStager.position4.get());
    SmartDashboard.putBoolean("Sensor 5", !robotStager.position5.get());

    SmartDashboard.putNumber("Turret Position", robotShooter.shooterPan.getSelectedSensorPosition());
    SmartDashboard.putNumber("Right Motor RPM", robotShooter.rightShooterEncoder.getVelocity());
    SmartDashboard.putNumber("Left Motor RPM", -robotShooter.leftShooterEncoder.getVelocity());
    SmartDashboard.putNumber("Vision DX", robotShooter.getDX());
    SmartDashboard.putNumber("Hood Position", robotShooter.hoodAngle.getSelectedSensorPosition());

    SmartDashboard.putNumber("Left Drive Position", robotDrive.driveLeftMaster.getSelectedSensorPosition());
    SmartDashboard.putNumber("Right Drive Position", robotDrive.driveRightMaster.getSelectedSensorPosition());
  }

  @Override
  public void testInit() {
  }

  @Override
  public void testPeriodic() {
  }
}
